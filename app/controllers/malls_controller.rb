class MallsController < ApplicationController
  before_action :set_mall, only: [:show, :edit, :update, :destroy]

  # GET /malls
  def index
    @malls = Mall.all
  end

  # GET /malls/1
  def show
  end

  # GET /malls/new
  def new
    @mall = Mall.new
  end

  # GET /malls/1/edit
  def edit
  end

  # POST /malls
  def create
    @mall = Mall.new(mall_params)

    if @mall.save
      redirect_to @mall, notice: 'Mall was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /malls/1
  def update
    if @mall.update(mall_params)
      redirect_to @mall, notice: 'Mall was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /malls/1
  def destroy
    @mall.destroy
    redirect_to malls_url, notice: 'Mall was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mall
      @mall = Mall.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def mall_params
      params.require(:mall).permit(:name)
    end
end
